import React, {Component} from 'react';

class CurrencyInput extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);

  }

  handleChange(event) {
    if (this.props.currency === 'USD') {
      this.props.setMoney(event.target.value);
    } else {
      this.props.setMoney(event.target.value / 6);
    }
  }

  render() {
    return (
      <section>
        <label>{this.props.currency}</label>
        <input
          type="text"
          onChange={this.handleChange}
          value={this.props.currency === 'USD' ? this.props.value : this.props.value * 6}
        />
      </section>
    )
  }
}

export default CurrencyInput;