import React, {Component} from 'react';
import CurrencyInput from "./CurrencyInput";

class CurrencyTranslator extends Component {

  constructor(props, context) {
    super(props, context);

    this.changeMoney = this.changeMoney.bind(this);

    this.state = {
      amount: 0
    }
  }

  changeMoney(money) {
    this.setState({amount: money})
  }

  render() {
    return (
      <div>
        <CurrencyInput setMoney={this.changeMoney} value={this.state.amount} currency="USD"/>
        <CurrencyInput setMoney={this.changeMoney} value={this.state.amount}  currency="CNY"/>
      </div>
    )
  }

}

export default CurrencyTranslator;